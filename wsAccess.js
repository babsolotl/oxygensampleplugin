function applicationStarted(pluginWorkspaceAccess) {

	/* https://bugs.openjdk.java.net/browse/JDK-8090517 */
	Packages.javafx.application.Platform.setImplicitExit(false);
	
	pluginPanel = new Packages.javax.swing.JPanel();
	jfxPanel = new Packages.javafx.embed.swing.JFXPanel();
	
	viewCustomizer = {
		customizeView: function (viewInfo) {
			if ("Sample Plugin" == viewInfo.getViewID()) {
				pluginPanel.setLayout(new Packages.javax.swing.BoxLayout(pluginPanel, Packages.javax.swing.BoxLayout.PAGE_AXIS));
				pluginPanel.setBorder(new Packages.javax.swing.border.EmptyBorder(10, 0, 0, 0));
				pluginPanel.add(jfxPanel);
				viewInfo.setComponent(pluginPanel);
				initWebView();
			}
		}
	}

	pluginWorkspaceAccess.addViewComponentCustomizer(viewCustomizer);
}

function applicationClosing(pluginWorkspaceAccess) {}

function initWebView() {

	Packages.javafx.application.Platform.runLater(
		new Packages.java.lang.Runnable({
			run: function () {
				createJAVAFXScene();
							
			}
		})
	);
	
}

function createJAVAFXScene() {

	sceneLayout = new Packages.javafx.scene.layout.StackPane();
	browser = new Packages.javafx.scene.web.WebView();
	browser.setContextMenuEnabled(false);
	webengine = browser.getEngine();
	webengine.load("https://www.google.com");
	sceneLayout.getChildren().add(browser);
	scene = new Packages.javafx.scene.Scene(sceneLayout);
	jfxPanel.setScene(scene);
	
}